/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/
#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>

typedef enum GameScreen { LOGO, TITLE, MENU, OPTIONS, CONTROLS, GAMEPLAY, ENDING, PRELOGO } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = PRELOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Color LogoColor = BLACK;
    LogoColor.a = 0;
    
    int counter = 0;
    int TitleSelection = 1;
    int OptionSelection = 1;
    int ControlSelection = 1;
    int GameMode = 1;
    int GameDifficulty = 2;
    Color MenusTextColor = BLACK;
    int SecretCodeStep = 0;
    int KeyTimeWindow = 60;
    bool DebugMode = true;
    int ModelH_Debug = 0;
    Color DebugModeTextColor = BLACK;
    int SpecialMode = 0;
    
    int BallDirectionRandValue;
    int BallDirectionRandNumbers = 1;
    time_t BallDirectionRandResult;
    
    srand((unsigned) time(&BallDirectionRandResult));
    
    bool FadeOut = true;
    float alpha = 0;
    float FadeSpeed = 0.01f;
    
    bool blink = false;
    bool pause = false;
    
    Rectangle player;
    int playerSpeedY = 8;
    
    Rectangle enemy;
    int enemySpeedY = 8;
    
    int MaxSpeed = 8;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenWidth/2;
    Vector2 ballSpeed;
    ballSpeed.x = 5;
    ballSpeed.y = 5;
    
    int BallXSpeed = ballSpeed.x;
    int BallYSpeed = ballSpeed.y;
    
    int ballRadius = 15;
    
    Vector2 PaddleSize;
    PaddleSize.x = 20;
    PaddleSize.y = 60;
    
    Vector2 RightPaddle, LeftPaddle;
    RightPaddle.x = screenWidth - 50 - PaddleSize.x;
    RightPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    LeftPaddle.x = 50;
    LeftPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    Vector2 WallSize;
    WallSize.x = screenWidth;
    WallSize.y = 20;
    
    Vector2 WallUp, WallDown;
    WallUp.x = 0;
    WallUp.y = 0;
    
    WallDown.x = 0;
    WallDown.y = screenHeight - WallSize.y;
    
    int IAspeedEasy = 3;
    int IALineEasy = screenWidth/2 + screenWidth/4;
    
    int IAspeed = 5;
    int IALine = screenWidth/2;
    
    int IAspeedHard = 7;
    int IALineHard = screenWidth/4;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int PlayerScore = 0;
    int IAScore = 0;
    int MaxScore = 10;
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, -1 - Not defined  
    
    int frameCount = 0;
    int frameCount2 = 0;
    int timeCounter = 0;
   
   
    Rectangle backRect1 = { 80, 30, 250, 30}; //Background Rectangle (X, Y, Anchura, Altura)
    int margin = 5;
    Rectangle fillRect1 = { backRect1.x + margin, backRect1.y + margin, backRect1.width - (2 * margin), backRect1.height - (2 * margin)};
    Rectangle lifeRect1 = fillRect1;
    Color lifeColor1 = WHITE;
    
    Rectangle backRect2 = { screenWidth/2 + 60, 30, 250, 30}; //Background Rectangle (X, Y, Anchura, Altura)
    Rectangle fillRect2 = { backRect2.x + margin, backRect2.y + margin, backRect2.width - (2 * margin), backRect2.height - (2 * margin)};
    Rectangle lifeRect2 = fillRect2;
    Color lifeColor2 = WHITE;
    
    int drainLife1 = lifeRect1.width / MaxScore;
    int drainLife2 = lifeRect2.width / MaxScore; //backRect2.width/MaxScore
    int LifeProcessMargin = 30;
    
    Vector2 SelectionMarker;
    SelectionMarker.x = 20;
    SelectionMarker.y = 20;
    
    
    
    //Model H sprites
    /*
    const int ModelH_spriteWidth = 85;
    const int ModelH_spriteHeight = 85;
    
    int ModelH_direction = 1;
    // 1. Right 2.Left
    int ModelH_animation = 5;
    /*
    1. Flying (Left)
    2. Dash (Left)
    3. Fly_Attack (Left)
    4. Dash_Attack (Left)
    5. Flying (Right)
    6. Dash (Right)
    7. Fly_Attack (Right)
    8. Dash_Attack (Right)
    */
    //int ModelH_Dashing = 0;
    //int ModelH_Attacking = 0;
    // 0. No 1. Yes
    /*
    Rectangle ModelH_sourceRec;
    ModelH_sourceRec.x = 0;
    ModelH_sourceRec.y = 0;
    ModelH_sourceRec.width = ModelH_spriteWidth;
    ModelH_sourceRec.height = ModelH_spriteHeight;
   
    Rectangle ModelH_destinationRec;
    ModelH_destinationRec.width = ModelH_sourceRec.width;
    ModelH_destinationRec.height = ModelH_sourceRec.height;
    
    Rectangle ModelH_DestinationRec;
    ModelH_destinationRec.x = ModelH_spriteWidth/2;
    ModelH_destinationRec.y = screenHeight/2 - ModelH_spriteHeight/2;
    
    Vector2 ModelH_Position;
    ModelH_Position.x = ModelH_destinationRec.x;
    ModelH_Position.y = ModelH_destinationRec.y;
   
    Vector2 ModelH_origin;
    ModelH_origin.x = 0;
    ModelH_origin.y = 0;
   
    int ModelH_framesCounter = 0;
    int ModelH_Dash_framesCounter = 0;
    int ModelH_Attack_framesCounter = 0;
    
    Vector2 ModelH_Speed;
    ModelH_Speed.x = 3;
    ModelH_Speed.y = 3;

    Vector2 ModelH_MaxSpeed;
    ModelH_MaxSpeed.x = 3;
    ModelH_MaxSpeed.y = 3;

    Vector2 ModelH_DashSpeed;
    ModelH_DashSpeed.x = 9;
    ModelH_DashSpeed.y = 9;

    Vector2 ModelH_MaxDashSpeed;
    ModelH_MaxDashSpeed.x = 9;
    ModelH_MaxDashSpeed.y = 9;
    */

    /*------- CUT CONTENT
    const int Pandora_Sprite_Width = 63;
    const int Pandora_Sprite_Height = 63;

    int ModelH_directionDelay = 0;
    int ModelH_directionDelaySwitch = 0;

    int Ball_LastPlayerContact = 0;
    // 1. Right 2. Left


    Rectangle Pandora_sourceRec;
    Pandora_sourceRec.x = 0;
    Pandora_sourceRec.y = 0;
    Pandora_sourceRec.width = Pandora_Sprite_Width;
    Pandora_sourceRec.height = Pandora_Sprite_Height;
   
    Rectangle Pandora_destinationRec;
    Pandora_destinationRec.width = Pandora_sourceRec.width;
    Pandora_destinationRec.height = Pandora_sourceRec.height;
    
    Rectangle Pandora_DestinationRec;
    Pandora_destinationRec.x = screenWidth/2 -Pandora_Sprite_Width/2;
    Pandora_destinationRec.y = screenHeight/2 - Pandora_Sprite_Height/2;
    
    Vector2 Pandora_origin;
    Pandora_origin.x = 0;
    Pandora_origin.y = 0;
    
    int Pandora_Animation = 0;
    Aniamtions:
    1. Appearing
    2. Ability
    3. Dissappearing
    */
    /*
    const int Barrier_Sprite_Width = 83;
    const int Barrier_Sprite_Height = 83; 
    
    int BarrierDirection = 1;
    
    int Barrier_Active = 0;
    // 0. Disabled 1. P1 Shield 2. P2 Shield 3. Both Shields
    
    Rectangle Barrier_sourceRec;
    Barrier_sourceRec.x = 0;
    Barrier_sourceRec.y = 0;
    Barrier_sourceRec.width = Barrier_Sprite_Width;
    Barrier_sourceRec.height = Barrier_Sprite_Height;
   
    Rectangle Barrier_destinationRec;
    Barrier_destinationRec.width = Barrier_sourceRec.width;
    Barrier_destinationRec.height = Barrier_sourceRec.height;
    
    Rectangle Barrier_DestinationRec;
    Barrier_destinationRec.x = screenWidth/2 - Barrier_Sprite_Width/2 - 300;
    Barrier_destinationRec.y = screenHeight/2 - Barrier_Sprite_Height/2;
    
    Vector2 Barrier_origin;
    Barrier_origin.x = 0;
    Barrier_origin.y = 0;
    
    Vector2 BarrierSize;
    BarrierSize.x = 20;
    BarrierSize.y = 60;
    
    Vector2 BarrierPosition;
    BarrierPosition.x = screenWidth - 50 - BarrierSize.x;
    BarrierPosition.y = screenHeight/2 - BarrierSize.y/2;
    
    
    int PowerupAppeared = 0;
    int PowerupActive = 0;
    
    Vector2 PowerupSize;
    PowerupSize.x = 20;
    PowerupSize.y = 20;
    
    Vector2 PowerupPosition;
    PowerupPosition.x = screenWidth/2;
    PowerupPosition.y = screenHeight/2;
    
    
    int ModelHBall_framesCounter = 0;
    
    const int ModelHBall_Sprite_Width = 53;
    const int ModelHBall_Sprite_Height = 53;
    
    Rectangle ModelHBall_sourceRec;
    ModelHBall_sourceRec.x = 0;
    ModelHBall_sourceRec.y = 0;
    ModelHBall_sourceRec.width = Barrier_Sprite_Width;
    ModelHBall_sourceRec.height = Barrier_Sprite_Height;
    

    Rectangle ModelHBall_DestinationRec;
    ModelHBall_DestinationRec.x = ball.x;
    ModelHBall_DestinationRec.y = ball.y;
    
    Vector2 ModelHBall_origin;
    ModelHBall_origin.x = 0;
    ModelHBall_origin.y = 0;
    
    int BallAspect = 0;
    //0.Regular ball 1.ModelH Projectile
    */
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Font fontTtf = LoadFontEx("Resources/04B_30__.ttf", 30, 0, 250);
    
    /*
    Texture2D ModelH_Flying_Left = LoadTexture("Resources/Model-H/Model-H-Flying-Left-85x85-B&W.png");
    Texture2D ModelH_Flying_Right = LoadTexture("Resources/Model-H/Model-H-Flying-Right-85x85-B&W.png");
    Texture2D ModelH_Dash_Left = LoadTexture("Resources/Model-H/Model-H-Dash-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Right = LoadTexture("Resources/Model-H/Model-H-Dash-Right-85x85-B&W.png");
    Texture2D ModelH_Flying_Attacking_Left = LoadTexture("Resources/Model-H/Model-H-Flying-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Flying_Attacking_Right = LoadTexture("Resources/Model-H/Model-H-Flying-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Attacking_Left = LoadTexture("Resources/Model-H/Model-H-Dash-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Attacking_Right = LoadTexture("Resources/Model-H/Model-H-Dash-Attacking-Right-85x85-B&W.png");
    
    Texture2D ModelHProjectile = LoadTexture("Resources/Model-H/Projectile-53x53-B&W.png");
    
    Texture2D Pandora_Appearence = LoadTexture("Resources/Pandora/Pandora-Appearence-63x63-B&W.png");
    Texture2D Pandora_Disappearence = LoadTexture("Resources/Pandora/Pandora-Disappearence-63x63-B&W.png");
    Texture2D Pandora_Ability = LoadTexture("Resources/Pandora/Pandora-Ability-63x63-B&W.png");
    
    Texture2D Barrier_Sprites = LoadTexture("Resources/Pandora/Barrier-83x83-B&W.png");
    */
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    InitAudioDevice();
    Sound Sound_Goal = LoadSound("Resources/Sounds/Goal.wav");
    Sound Sound_Bounce = LoadSound("Resources/Sounds/Bounce.wav");
    Sound Sound_Logo = LoadSound("Resources/Sounds/Capcom-SNES-Game-Intro.mp3");
    Music Music_Menu = LoadMusicStream("Resources/Sounds/Road-Less-Taken-8-Bit-VRC6.mp3");
    Music Music_OptionsControls = LoadMusicStream("Resources/Sounds/Invitation-to-Freedom-8-Bit-VRC6.mp3");
    Music Music_Gameplay = LoadMusicStream("Resources/Sounds/Rude-Buster-8-Bit-VRC6.mp3");
    Music Music_Ending = LoadMusicStream("Resources/Sounds/THE-WORLD-REVOLVING-8-Bit-VRC6.mp3");
    Music Music_Title = LoadMusicStream("Resources/Sounds/Bloody-Stream-8-Bit-VRC6.mp3");
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        UpdateMusicStream(Music_Menu);
        UpdateMusicStream(Music_OptionsControls);
        UpdateMusicStream(Music_Gameplay);
        UpdateMusicStream(Music_Title);
        
        switch(screen) 
        {
            case PRELOGO:
            {
                PlaySound(Sound_Logo);
                screen = LOGO;
                
            }break;
            
            case LOGO: 
            {
                // Update LOGO screen data here!
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
                
                if (LogoColor.a < 255){
                    LogoColor.a++;
                }
                    
                    if (framesCounter > 180){
                        screen = TITLE;
                        framesCounter = 0;
                    }
               
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                PlayMusicStream(Music_Title);
                if (IsKeyPressed(KEY_O)){
                    DebugMode = !DebugMode;
                }
                
                // TODO: Title animation logic.......................(0.5p)
                framesCounter++;
                if (framesCounter % 30 == 0){
                    framesCounter = 0;
                    blink = !blink;
                }
                
                if (IsKeyDown(KEY_UP)){
                    SecretCodeStep = 1;
                }else if (IsKeyDown(KEY_UP) && SecretCodeStep == 1){
                    SecretCodeStep = 2;
                }else if (IsKeyDown(KEY_DOWN) && SecretCodeStep == 2){
                    SecretCodeStep = 3;
                }else if (IsKeyDown(KEY_DOWN) && SecretCodeStep == 3){
                    SecretCodeStep = 4;
                }else if (IsKeyDown(KEY_LEFT) && SecretCodeStep == 4){
                    SecretCodeStep = 5;
                }else if (IsKeyDown(KEY_LEFT) && SecretCodeStep == 5){
                    SecretCodeStep = 6;
                }else if (IsKeyDown(KEY_RIGHT) && SecretCodeStep == 6){
                    SecretCodeStep = 7; 
                }else if (IsKeyDown(KEY_LEFT) && SecretCodeStep == 7){
                    SecretCodeStep = 8;
                }else if (SecretCodeStep == 8){
                    screen = TITLE;
                }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
                    screen = MENU;
                }
                
            } break;
            
            case MENU:
            {
                if (IsKeyPressed(KEY_O)){
                    DebugMode = !DebugMode;
                }
                
                PlayMusicStream(Music_Menu);
                StopMusicStream(Music_OptionsControls); 
                StopMusicStream(Music_Gameplay);
                StopMusicStream(Music_Title);
                
                if (counter <= 10){
                    counter++;
                }else{
                
                       if (IsKeyPressed(KEY_DOWN)){
                           TitleSelection++;
                       }
                       
                       if (IsKeyPressed(KEY_UP)){
                           TitleSelection--;
                       }
                       
                       if (TitleSelection < 1){
                           TitleSelection = 1;
                       }else if(TitleSelection > 4){
                           TitleSelection = 4;
                       }
                       
                           if (TitleSelection == 1){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = GAMEPLAY;
                                   counter = 0; 
                               }
                           }
                           
                           if (TitleSelection == 2){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = OPTIONS;
                                   counter = 0;
                                   OptionSelection = 1;
                               }
                           }
                           
                           if (TitleSelection == 3){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = CONTROLS;
                                   counter = 0;
                                   ControlSelection = 1;
                               }
                           }
                          
                           if (TitleSelection == 4){
                               if (IsKeyPressed(KEY_ENTER)){
                                   return 0;
                               }
                           }
                }
            }break;
            
            case OPTIONS:
            {
                if (IsKeyPressed(KEY_O)){
                    DebugMode = !DebugMode;
                }
                
                PlayMusicStream(Music_OptionsControls);
                StopMusicStream(Music_Menu);
                StopMusicStream(Music_Gameplay);
                
                if (counter <= 10){
                    counter++;
                }else{
                   if (OptionSelection < 1){
                       OptionSelection = 1;
                   }else if(OptionSelection > 4){
                       OptionSelection = 4;
                   }

                   if (IsKeyPressed(KEY_DOWN)){
                       OptionSelection++;
                   }
                   if (IsKeyPressed(KEY_UP)){
                       OptionSelection--;
                   }
                   
                   if (OptionSelection < 1){
                       OptionSelection = 1;
                   }else if(OptionSelection > 4){
                       OptionSelection = 4;
                   }
                   
                       if (OptionSelection == 1){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameMode = 1;
                           }
                       }else if (OptionSelection == 2){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameDifficulty++;
                               if (GameDifficulty > 3){
                                   GameDifficulty = 1;
                               }
                           }
                       }else if (OptionSelection == 3){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameMode = 2;;
                           }
                       }else if (OptionSelection == 4){
                           if (IsKeyPressed(KEY_ENTER)){
                               screen = MENU;
                               counter = 0;
                           }
                       }
                }   
            }break;
            case CONTROLS:
            {
                if (IsKeyPressed(KEY_O)){
                    DebugMode = !DebugMode;
                }
                PlayMusicStream(Music_OptionsControls);
                StopMusicStream(Music_Menu);
                if (counter <= 10){
                    counter++;
                }else{
                    if (ControlSelection == 1){
                        if (IsKeyPressed(KEY_ENTER)){
                            screen = MENU;
                        }
                    }
                }
            }break;
            case GAMEPLAY:
            { 
              // Update GAMEPLAY screen data here!
              if (IsKeyPressed(KEY_O)){
                  DebugMode = !DebugMode;
              }
              /* 
              if (IsKeyPressed(KEY_H)){
                  if (ModelH_Debug == 0){
                      ModelH_Debug = 1;
                  }else if (ModelH_Debug == 1){
                      ModelH_Debug = 0;
                  }
              }
              if (ModelH_directionDelaySwitch == 0){
                    ModelH_directionDelay++;
                    if (ModelH_directionDelay >= 20){
                        ModelH_directionDelay = 20;
                        ModelH_directionDelaySwitch = 1;
                    }
              }
              
              if (BallAspect == 1){
                  ModelHBall_framesCounter++;
                    if (ModelHBall_framesCounter >= 6){
                        if (ModelHBall_sourceRec.x > ModelHProjectile.width);
                        {
                            ModelHBall_sourceRec.x = 0;
                        }
                        ModelHBall_framesCounter = 0;
                        ModelHBall_sourceRec.x += ModelHBall_Sprite_Width;
                    }
              }
              */
              
              if (!pause){
                // TODO: Ball movement logic.........................(0.2p)
                PlayMusicStream(Music_Gameplay);
                StopMusicStream(Music_Menu);
                StopMusicStream(Music_OptionsControls);
                
                ball.x += ballSpeed.x;
                ball.y += ballSpeed.y;
                
                for (BallDirectionRandValue = 0 ; BallDirectionRandValue < BallDirectionRandNumbers ; BallDirectionRandValue++){
                    (BallDirectionRandResult = rand() % 2);
                }
                
                if (ball.x <= 0){
                    IAScore ++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    secondsCounter = 99;
                    lifeRect1.width -= drainLife1;
                    PlaySound(Sound_Goal);
                    if (BallDirectionRandResult == 0){
                        ballSpeed.x = 5;
                        ballSpeed.y = 5;
                    }else if (BallDirectionRandResult == 1){
                        ballSpeed.x = -5;
                        ballSpeed.y = -5;
                    }
                    
                }else if (ball.x >= screenWidth){
                    PlayerScore ++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    secondsCounter = 99;
                    lifeRect2.width -= drainLife2;
                    PlaySound(Sound_Goal);
                    if (BallDirectionRandResult == 0){
                        ballSpeed.x = 5;
                        ballSpeed.y = 5;
                    }else if (BallDirectionRandResult == 1){
                        ballSpeed.x = -5;
                        ballSpeed.y = -5;
                    }
                }
                
                // TODO: Player movement logic.......................(0.2p)
                
                if (IsKeyDown(KEY_Q)){
                    LeftPaddle.y -= playerSpeedY;
                }else if (IsKeyDown(KEY_A)){
                    LeftPaddle.y += playerSpeedY;
                }     
                
                if (GameMode == 2){
                    if (IsKeyDown(KEY_UP)){
                        RightPaddle.y -= playerSpeedY;
                    }else if (IsKeyDown(KEY_DOWN)){
                        RightPaddle.y += playerSpeedY;
                    }
                }
               
                if (LeftPaddle.y < 20){
                    LeftPaddle.y = 20;
                }else if (LeftPaddle.y > screenHeight - PaddleSize.y - 20){
                    LeftPaddle.y = screenHeight - PaddleSize.y - 20;
                }
                
                if (RightPaddle.y < 20){
                    RightPaddle.y = 20;   
                }else if (RightPaddle.y > screenHeight - PaddleSize.y - 20){
                    RightPaddle.y = screenHeight - PaddleSize.y - 20;
                }

                // TODO: Enemy movement logic (IA)...................(1p)
                if (GameMode == 1){
                    if (GameDifficulty == 1){
                        if (ball.x > IALineEasy){
                            if (ball.y > RightPaddle.y){
                                RightPaddle.y += IAspeedEasy;
                            }else if (ball.y < RightPaddle.y){
                                RightPaddle.y -= IAspeedEasy;
                            }
                        }
                    }
                    if (GameDifficulty == 2){
                        if (ball.x > IALine){
                            if (ball.y > RightPaddle.y){
                                RightPaddle.y += IAspeed;
                            }else if (ball.y < RightPaddle.y){
                                RightPaddle.y -= IAspeed;
                            }
                        }
                    }
                    if (GameDifficulty == 3){
                        if (ball.x > IALineHard){
                            if (ball.y > RightPaddle.y){
                                RightPaddle.y += IAspeedHard;
                            }else if (ball.y < RightPaddle.y){
                                RightPaddle.y -= IAspeedHard;
                            }
                        }
                    }
                }
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){LeftPaddle.x - PaddleSize.x/2, LeftPaddle.y - PaddleSize.y/2 + 10, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x < 0){
                        PlaySound(Sound_Bounce);
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.1;
                            ballSpeed.y *= 1.1;
                        }else{
                            ballSpeed.x *= -1.1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){RightPaddle.x - PaddleSize.x/2, RightPaddle.y - PaddleSize.y/2 + 10, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x > 0){
                        PlaySound(Sound_Bounce);
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.1;
                            ballSpeed.y *= 1.1;
                        }else{
                            ballSpeed.x *= -1.1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if (ball.y >= screenHeight - ballRadius - WallSize.y || ball.y < 0 + ballRadius + WallSize.y){
                    ballSpeed.y *= -1;
                    PlaySound(Sound_Bounce);
                }
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                //Model H programming --- CUT CONTENT
                
                /*
                if (ModelH_destinationRec.x <= 0){
                    if (ModelH_directionDelaySwitch == 1){
                        if (ModelH_direction == 1){
                            if (ModelH_Dashing == 0){
                                ModelH_animation = 1;
                            }
                            ModelH_direction = 2;
                            ModelH_Speed.x *= -1;
                            ModelH_DashSpeed.x *= -1;
                        }else if (ModelH_direction == 2){
                            if (ModelH_Dashing == 0){
                                ModelH_animation = 5;
                            }
                            ModelH_direction = 1;
                            ModelH_Speed.x *= -1;
                            ModelH_DashSpeed.x *= -1;
                        }
                        ModelH_directionDelaySwitch = 0;
                        ModelH_directionDelay = 0;
                    }
                    
                }else if (ModelH_destinationRec.x >= screenWidth - ModelH_spriteWidth){
                    if (ModelH_directionDelaySwitch == 1){    
                        if (ModelH_direction == 1){
                            if (ModelH_Dashing == 0){
                                ModelH_animation = 1;
                            }
                            ModelH_direction = 2;
                            ModelH_Speed.x *= -1;
                            ModelH_DashSpeed.x *= -1;
                        }else if (ModelH_direction == 2){
                            if (ModelH_Dashing == 0){
                                ModelH_animation = 5;
                            }
                            ModelH_direction = 1;
                            ModelH_Speed.x *= -1;
                            ModelH_DashSpeed.x *= -1;
                        }
                        ModelH_directionDelaySwitch = 0;
                        ModelH_directionDelay = 0;
                    }    
                }
                
                if (ModelH_destinationRec.y <= 0){
                    if (ModelH_directionDelaySwitch == 1){
                        if (ModelH_direction == 1){
                            ModelH_direction = 2;
                            ModelH_Speed.y *= -1;
                            ModelH_DashSpeed.y *= -1;
                        }else if (ModelH_direction == 2){
                            ModelH_direction = 1;
                            ModelH_Speed.y *= -1;
                            ModelH_DashSpeed.y *= -1;
                        }
                        ModelH_directionDelaySwitch = 0;
                        ModelH_directionDelay = 0;
                    }
                }else if (ModelH_destinationRec.y >= screenHeight - ModelH_spriteHeight){
                    if (ModelH_directionDelaySwitch == 1){
                        if (ModelH_direction == 1){
                            ModelH_direction = 2;
                            ModelH_Speed.y *= -1;
                            ModelH_DashSpeed.y *= -1;
                        }else if (ModelH_direction == 2){
                            ModelH_direction = 1;
                            ModelH_Speed.y *= -1;
                            ModelH_DashSpeed.y *= -1;
                        }
                        ModelH_directionDelaySwitch = 0;
                        ModelH_directionDelay = 0;
                    }
                }
                
                
                if (ModelH_destinationRec.x == ball.x){
                    
                    if (ballSpeed.x > 0){
                        ModelH_animation = 3;
                    }else if (ballSpeed.x < 0){
                        ModelH_animation = 7;
                    }
                    
                }else if (ModelH_destinationRec.y == ball.y){
                    
                    if (ballSpeed.x > 0){
                        ModelH_animation = 3;
                    }else if (ballSpeed.x < 0){
                        ModelH_animation = 7;
                    }
                    
                }
                
                if (ModelH_animation <= 4){
                    ModelH_direction = 2;
                    ModelH_destinationRec.x -= ModelH_Speed.x;
                    ModelH_destinationRec.y -= ModelH_Speed.y;
                    if (ModelH_Position.x <= 0){
                        ModelH_Speed.x = 3;
                        ModelH_Speed.y = 3;
                    }else if (ModelH_Position.x >= screenWidth){
                        ModelH_Speed.x = 3;
                        ModelH_Speed.y = 3;
                    }
                }else if (ModelH_animation >=5){
                    ModelH_direction = 1;
                    ModelH_destinationRec.x += ModelH_Speed.x;
                    ModelH_destinationRec.y += ModelH_Speed.y;
                    if (ModelH_Position.x <= 0){
                        ModelH_Speed.x = 3;
                        ModelH_Speed.y = 3;
                    }else if (ModelH_Position.x >= screenWidth){
                        ModelH_Speed.x = 3;
                        ModelH_Speed.y = 3;
                    }
                }
                
                if (ModelH_animation == 1){
                    
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Flying_Left.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 2){;

                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Dash_Left.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 3){
                   
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Flying_Attacking_Left.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 4){
                    
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Dash_Attacking_Left.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 5){
                    
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Flying_Right.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 6){
                    
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Dash_Right.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 7){
                    
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Flying_Attacking_Right.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }else if (ModelH_animation == 8){ 
                   
                    ModelH_framesCounter++;
                    if (ModelH_framesCounter >= 4){
                        if (ModelH_sourceRec.x > ModelH_Dash_Attacking_Right.width);
                        {
                            ModelH_sourceRec.x = 0;
                        }
                        ModelH_framesCounter = 0;
                        ModelH_sourceRec.x += ModelH_spriteWidth;
                    }
                    
                }
                */      
                
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                
                /*
                Pandora + Barrier programming ---- CUT CONTENT
                
                if (PowerupActive == 1){
                    
                    if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){BarrierPosition.x - BarrierSize.x/2, BarrierSize.y - BarrierSize.y/2 + 10, BarrierSize.x, BarrierSize.y})){
                        PowerupActive == 0;  
                    }
                    
                    if (BarrierPosition.y >= 30){
                        BarrierDirection = 2;
                    }else if (BarrierPosition.y == screenHeight - 30){
                        BarrierDirection = 1;
                    }
                    
                    if (BarrierDirection == 1){
                        BarrierPosition.y ++;
                    }else if (BarrierDirection == 2){
                        BarrierPosition.y --;
                    }
                } 
                */
                
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                
                
                // TODO: Life bars decrease logic....................(1p)
                // El codigo se encuentra en el apartado de movimiento de la bola, al realizar un gol, ya que sino no funcionaba correctamente.
             
                // TODO: Time counter logic..........................(0.2p)
                
                if (secondsCounter > 0){
                    framesCounter++;
                    if (framesCounter == 60){
                    secondsCounter--;
                    framesCounter = 0;
                    }
                }else{
                    screen = ENDING;
                }
                
                // TODO: Game ending logic...........................(0.2p)
                
                if (PlayerScore >= MaxScore || IAScore >= MaxScore){
                    screen = ENDING;
                }else if (secondsCounter <= 0){
                    screen = ENDING;
                }
              }
              // TODO: Pause button logic..........................(0.2p)
              if (IsKeyPressed(KEY_P)){
                    pause = !pause;
                }

            } break;
            case ENDING: 
            {
                // Update END screen data here!
            if (IsKeyPressed(KEY_O)){
                DebugMode = !DebugMode;
            }
                // TODO: Replay / Exit game logic....................(0.5p)
                
                if (IsKeyPressed(KEY_R)){
                    screen = GAMEPLAY;
                    pause = false;
                    PlayerScore = 0;
                    IAScore = 0;
                    ballSpeed.x = 8;
                    ballSpeed.y = 8;
                    lifeRect1.width = backRect1.width - (2 * margin);
                    lifeRect2.width = backRect2.width - (2 * margin);
                }else if (IsKeyPressed(KEY_ENTER)){
                    return 0;
                }
                
            } break;
            default: break;
        }
        
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawText("Roger Font", 160, 200, 80, LogoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawText("FINAL PONG", 150, 150, 80, LogoColor);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (!blink){
                        DrawText("PRESS ENTER TO START", 160, 250, 40, LogoColor);
                    }
                   
                    if (!DebugMode){
                    DrawTextEx(fontTtf, FormatText("Debug Mode ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Counter: %d", counter), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("TitleSelection: %d", TitleSelection), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("OptionSelection: %d", OptionSelection), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("ControlSelection: %d", ControlSelection), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameMode: %d", GameMode), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameDifficulty: %d", GameDifficulty), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                    }
                    
                } break;

                case MENU:
                {
                    DrawTextEx(fontTtf, FormatText("PONG"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 100, 0, MenusTextColor);
                    DrawTextEx(fontTtf, FormatText("by Roger Font"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 60}, 20, 0, MenusTextColor);
                    DrawText("Start game", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                    DrawText("Options", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                    DrawText("Controls", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    DrawText("Exit Game", screenWidth/2 - 200, screenHeight/2 + 200, 20, MenusTextColor);
                    
                    if (TitleSelection == 1){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 50}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 2){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 100}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 3){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 4){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 200}, SelectionMarker, MenusTextColor);
                    }
                    
                    if (!DebugMode){
                    DrawTextEx(fontTtf, FormatText("Debug Mode ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Counter: %d", counter), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("TitleSelection: %d", TitleSelection), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("OptionSelection: %d", OptionSelection), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("ControlSelection: %d", ControlSelection), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameMode: %d", GameMode), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameDifficulty: %d", GameDifficulty), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                        if (GameMode == 1){
                            if (GameDifficulty == 1){
                                DrawLine(IALineEasy, 0, IALineEasy , screenHeight, RED);
                            }else if (GameDifficulty == 2){
                                DrawLine(IALine, 0, IALine , screenHeight, RED);
                            }else if (GameDifficulty == 3){
                                DrawLine(IALineHard, 0, IALineHard , screenHeight, RED);
                            }
                        }
                    }                    
                }break;
                case OPTIONS:
                {
                    DrawTextEx(fontTtf, FormatText("OPTIONS"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 60, 0, MenusTextColor);
                    DrawText("Back", screenWidth/2 - 200, screenHeight/2 + 200, 20, MenusTextColor);
                    
                    if (GameMode == 1){
                        DrawText("Mode Select: Vs AI (Selected)", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                        DrawText("Mode Select: Multiplayer", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    }else if (GameMode == 2){
                        DrawText("Mode Select: Vs AI", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                        DrawText("Mode Select: Multiplayer (Selected)", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    }
                    
                    if (GameDifficulty == 1){
                            DrawText("AI Difficulty: Easy", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }else if (GameDifficulty == 2){
                            DrawText("AI Difficulty: Normal", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }else if (GameDifficulty == 3){
                            DrawText("AI Difficulty: Hard", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }
                    
                    if (OptionSelection == 1){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 50}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 2){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 100}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 3){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 4){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 200}, SelectionMarker, MenusTextColor);                        
                    }
                    
                    if (!DebugMode){
                    DrawTextEx(fontTtf, FormatText("Debug Mode ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Counter: %d", counter), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("TitleSelection: %d", TitleSelection), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("OptionSelection: %d", OptionSelection), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("ControlSelection: %d", ControlSelection), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameMode: %d", GameMode), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameDifficulty: %d", GameDifficulty), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                    if (GameMode == 1){
                            if (GameDifficulty == 1){
                                DrawLine(IALineEasy, 0, IALineEasy , screenHeight, RED);
                            }else if (GameDifficulty == 2){
                                DrawLine(IALine, 0, IALine , screenHeight, RED);
                            }else if (GameDifficulty == 3){
                                DrawLine(IALineHard, 0, IALineHard , screenHeight, RED);
                            }
                        }
                    }
                }break;
                case CONTROLS:
                {
                    DrawTextEx(fontTtf, FormatText("CONTROLS"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 60, 0, MenusTextColor);
                    DrawText("P2: Up arrow = up / Down arrow = down", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                    DrawText("P1: Q = up / A = down", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                    DrawText("Back", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    if (ControlSelection == 1){
                    DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }
                    if (!DebugMode){
                    DrawTextEx(fontTtf, FormatText("Debug Mode ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Counter: %d", counter), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("TitleSelection: %d", TitleSelection), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("OptionSelection: %d", OptionSelection), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("ControlSelection: %d", ControlSelection), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameMode: %d", GameMode), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameDifficulty: %d", GameDifficulty), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                    }
                }break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    DrawRectangleV(WallUp, WallSize, BLACK);
                    DrawRectangleV(WallDown, WallSize, BLACK);
                    
                    DrawCircleV(ball, ballRadius, BLACK); 
                    /*
                    if (BallAspect == 0){
                        DrawCircleV(ball, ballRadius, BLACK);
                    }else if (BallAspect == 1){
                        DrawTexturePro(ModelHProjectile, ModelHBall_sourceRec, ModelHBall_DestinationRec, ModelHBall_origin, 0, WHITE);
                    }
                    */
                    // Model H sprites:
                    /*
                    if (ModelH_animation == 1){
                        DrawTexturePro(ModelH_Flying_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 2){
                        DrawTexturePro(ModelH_Dash_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 3){
                        DrawTexturePro(ModelH_Flying_Attacking_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 4){
                        DrawTexturePro(ModelH_Dash_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 5){
                        DrawTexturePro(ModelH_Flying_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 6){
                        DrawTexturePro(ModelH_Dash_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 7){
                        DrawTexturePro(ModelH_Flying_Attacking_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 8){
                        DrawTexturePro(ModelH_Dash_Attacking_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    } 


                    if (PowerupAppeared == 1){
                        DrawRectangleV(PowerupPosition, PowerupSize, BLACK);
                    }
                    

                    Pandora + Barrier sprites
                    if (Pandora_Animation == 1){
                        DrawTexturePro(Pandora_Appearence, Pandora_sourceRec, Pandora_destinationRec, Pandora_origin, 0, WHITE);
                    }else if (Pandora_Animation == 2){
                        DrawTexturePro(Pandora_Disappearence, Pandora_sourceRec, Pandora_destinationRec, Pandora_origin, 0, WHITE);
                    }else if (Pandora_Animation == 3){
                        DrawTexturePro(Pandora_Ability, Pandora_sourceRec, Pandora_destinationRec, Pandora_origin, 0, WHITE);
                    }
                          
                    if (Barrier_Active == 1){
                        DrawTexturePro(Barrier_Sprites, Barrier_sourceRec, Barrier_destinationRec, Barrier_origin, 0, WHITE);
                    }
                    */
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleV(LeftPaddle, PaddleSize, BLACK);
                    DrawRectangleV(RightPaddle, PaddleSize, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec (backRect1, BLACK);
                    DrawRectangleRec (fillRect1, BLACK);
                    DrawRectangleRec (lifeRect1, lifeColor1);
                   
                    DrawRectangleRec (backRect2, BLACK);
                    DrawRectangleRec (fillRect2, BLACK);
                    DrawRectangleRec (lifeRect2, lifeColor1);
                    
                    DrawTextEx(fontTtf, FormatText("%d", PlayerScore), (Vector2){20, 20}, 50, 0, BLACK);
                    DrawTextEx(fontTtf, FormatText("%d", IAScore), (Vector2){screenWidth - 70, 20}, 50, 0, BLACK);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawTextEx(fontTtf, FormatText("%d", secondsCounter), (Vector2){screenWidth/2 - 45, 20}, 50, 0, BLACK);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause == true){
                        DrawText("Game paused, press P to continue", 150, 150, 30, BLACK);
                    }
                    
                    if (!DebugMode){
                    DrawTextEx(fontTtf, FormatText("Debug Mode ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Counter: %d", counter), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("LifeProcessMargin: %d", LifeProcessMargin), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Direction result: %d", BallDirectionRandResult), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("ballSpeed.y: %d", BallYSpeed), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameMode: %d", GameMode), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("GameDifficulty: %d", GameDifficulty), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                        if (GameMode == 1){
                            if (GameDifficulty == 1){
                                DrawLine(IALineEasy, 0, IALineEasy , screenHeight, RED);
                            }else if (GameDifficulty == 2){
                                DrawLine(IALine, 0, IALine , screenHeight, RED);
                            }else if (GameDifficulty == 3){
                                DrawLine(IALineHard, 0, IALineHard , screenHeight, RED);
                            }
                        }
                    }
                    /*
                    if (ModelH_Debug = 1){
                    DrawTextEx(fontTtf, FormatText("Model H debug ON", counter), (Vector2){screenWidth - 300, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Animation: %d", ModelH_animation), (Vector2){10, 20}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Direction: %d", ModelH_direction), (Vector2){10, 60}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("FramesCounter: %d", ModelH_framesCounter), (Vector2){10, 80}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Speed: %d", ModelH_Speed), (Vector2){10, 100}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Dash Speed: %d", ModelH_DashSpeed), (Vector2){10, 120}, 20, 0, DebugModeTextColor);
                    DrawTextEx(fontTtf, FormatText("Direction Delay Switch: %d", ModelH_directionDelaySwitch), (Vector2){10, 140}, 20, 0, DebugModeTextColor);
                    }
                    */
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    if (PlayerScore >= MaxScore){
                        DrawText("Player WINS!", 150, 150, 30, BLACK);
                        DrawText("Press R to do another match!", 150, 200, 30, BLACK);
                        DrawText("or Press Enter close the game", 150, 250, 30, BLACK);
                    }else if (IAScore >= MaxScore){
                        DrawText("AI WINS!", 150, 150, 30, BLACK);
                        DrawText("Press R to do another match!", 150, 200, 30, BLACK);
                        DrawText("or Press Enter close the game", 150, 250, 30, BLACK);
                    }
                    
                    if (secondsCounter <= 0){
                        DrawText("TIME RAN OUT!", 150, 150, 30, BLACK);
                    }
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    UnloadFont (fontTtf);
    
    /*
    UnloadTexture(ModelH_Flying_Left);
    UnloadTexture(ModelH_Flying_Right);
    UnloadTexture(ModelH_Dash_Left);
    UnloadTexture(ModelH_Dash_Right);
    UnloadTexture(ModelH_Flying_Attacking_Left);
    UnloadTexture(ModelH_Flying_Attacking_Right);
    UnloadTexture(ModelH_Dash_Attacking_Left);
    UnloadTexture(ModelH_Dash_Attacking_Right);
    
    UnloadTexture(ModelHProjectile);

    UnloadTexture(Pandora_Appearence);
    UnloadTexture(Pandora_Disappearence);
    UnloadTexture(Pandora_Ability);
    
    UnloadTexture(Barrier_Sprites);
    */
    
    UnloadSound(Sound_Goal);
    UnloadSound(Sound_Bounce);
    UnloadSound(Sound_Logo);
    UnloadMusicStream(Music_Menu);
    UnloadMusicStream(Music_OptionsControls);
    UnloadMusicStream(Music_Gameplay);
    UnloadMusicStream(Music_Ending);
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}