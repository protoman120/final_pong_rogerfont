/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = GAMEPLAY;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Color LogoColor = BLACK;
    LogoColor.a = 0;
    
    bool FadeOut = true;
    float alpha = 0;
    float FadeSpeed = 0.01f;
    
    bool blink = false;
    
    Rectangle player;
    int playerSpeedY = 8;
    
    Rectangle enemy;
    int enemySpeedY = 8;
    
    int MaxSpeed = 8;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenWidth/2;
    Vector2 ballSpeed;
    ballSpeed.x = 8;
    ballSpeed.y = 8;
    
    int ballRadius = 25;
    
    Vector2 PaddleSize;
    PaddleSize.x = 20;
    PaddleSize.y = 100;
    
    Vector2 RightPaddle, LeftPaddle;
    RightPaddle.x = screenWidth - 50 - PaddleSize.x;
    RightPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    LeftPaddle.x = 50;
    LeftPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
                
                if (LogoColor.a < 255){
                    LogoColor.a++;
                }
                    
                    if (framesCounter > 180){
                        screen = TITLE;
                        framesCounter = 0;
                    }
               
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                framesCounter++;
                if (framesCounter % 30 == 0){
                    framesCounter = 0;
                    blink = !blink;
                }
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
                    screen = GAMEPLAY;
                }
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                
                

                // TODO: Ball movement logic.........................(0.2p)
                
                ball.x += ballSpeed.x;
                ball.y += ballSpeed.y;
                
                // TODO: Player movement logic.......................(0.2p)
                
                if (IsKeyDown(KEY_Q)){
                    LeftPaddle.y -= playerSpeedY;
                }else if (IsKeyDown(KEY_A)){
                    LeftPaddle.y += playerSpeedY;
                }
                
                if (IsKeyDown(KEY_UP)){
                    RightPaddle.y -= playerSpeedY;
                }else if (IsKeyDown(KEY_DOWN)){
                    RightPaddle.y += playerSpeedY;
                }
               
                if (LeftPaddle.y < 0){
                    LeftPaddle.y = 0;
                }else if (LeftPaddle.y > screenHeight){
                    LeftPaddle.y = screenHeight;
                }
                
                if (RightPaddle.y < 0){
                    RightPaddle.y = 0;   
                }else if (RightPaddle.y > screenHeight){
                    RightPaddle.y = screenHeight;
                }

                // TODO: Enemy movement logic (IA)...................(1p)
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if (ball.y >= screenHeight - ballRadius || ball.y < 0 + ballRadius){
                    ballSpeed.y *= -1;
                }
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){LeftPaddle.x - PaddleSize.x/2, LeftPaddle.y - PaddleSize.y/2, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x < 0){
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.5;
                            ballSpeed.y *= 1.5;
                        }else{
                            ballSpeed.x *= -1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){RightPaddle.x - PaddleSize.x/2, RightPaddle.y - PaddleSize.y/2, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x > 0){
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.5;
                            ballSpeed.y *= 1.5;
                        }else{
                            ballSpeed.x *= -1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawText("Roger Font", 160, 200, 80, LogoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawText("FINAL PONG", 150, 150, 80, LogoColor);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (!blink){
                        DrawText("PRESS ENTER TO START", 160, 250, 40, LogoColor);
                    }
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    DrawCircleV(ball, ballRadius, BLACK);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleV(LeftPaddle, PaddleSize, BLACK);
                    DrawRectangleV(RightPaddle, PaddleSize, BLACK);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}