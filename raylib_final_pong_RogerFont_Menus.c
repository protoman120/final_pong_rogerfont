/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/
#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, MENU, OPTIONS, CONTROLS, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Color LogoColor = BLACK;
    LogoColor.a = 0;
    
    int counter = 0;
    int TitleSelection = 1;
    int OptionSelection = 1;
    int ControlSelection = 1;
    int GameMode = 1;
    int GameDifficulty = 1;
    Color MenusTextColor = BLACK;
    
    bool FadeOut = true;
    float alpha = 0;
    float FadeSpeed = 0.01f;
    
    bool blink = false;
    bool pause = false;
    
    Rectangle player;
    int playerSpeedY = 8;
    
    Rectangle enemy;
    int enemySpeedY = 8;
    
    int MaxSpeed = 8;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenWidth/2;
    Vector2 ballSpeed;
    ballSpeed.x = 8;
    ballSpeed.y = 8;
    
    int ballRadius = 15;
    
    Vector2 PaddleSize;
    PaddleSize.x = 20;
    PaddleSize.y = 60;
    
    Vector2 RightPaddle, LeftPaddle;
    RightPaddle.x = screenWidth - 50 - PaddleSize.x;
    RightPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    LeftPaddle.x = 50;
    LeftPaddle.y = screenHeight/2 - PaddleSize.y/2;
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    Vector2 WallSize;
    WallSize.x = screenWidth;
    WallSize.y = 20;
    
    Vector2 WallUp, WallDown;
    WallUp.x = 0;
    WallUp.y = 0;
    
    WallDown.x = 0;
    WallDown.y = screenHeight - WallSize.y;
    
    int IAspeed = 2;
    int IALine = screenWidth/2;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int PlayerScore = 0;
    int IAScore = 0;
    int MaxScore = 10;
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, -1 - Not defined
    
    Vector2 LifeSizePlayer;
    LifeSizePlayer.x = 100;
    LifeSizePlayer.y = 30;
    
    Vector2 LifePlayer;
    LifePlayer.x = 10 + LifeSizePlayer.x;
    LifePlayer.y = LifeSizePlayer.y;
    
    Vector2 LifeSizePlayerOriginal;
    LifeSizePlayerOriginal.x = 10 + LifeSizePlayer.x;
    LifeSizePlayerOriginal.y = LifeSizePlayer.y;
    
    Color LifePlayerColor;
    
    int LifeDrainPlayer = LifePlayer.x - (LifeSizePlayerOriginal.x / MaxScore);
    
    Vector2 LifeSizeEnemy;
    LifeSizeEnemy.x = 100;
    LifeSizeEnemy.y = 30;
    
    Vector2 LifeEnemy;
    LifeEnemy.x = screenWidth/2 + LifeSizeEnemy.x;
    LifeEnemy.y = LifeSizeEnemy.y;
    
    Vector2 LifeSizeEnemyOriginal;
    LifeSizeEnemyOriginal.x = screenWidth/2 + LifeSizeEnemy.x;
    LifeSizeEnemyOriginal.y = LifeSizeEnemy.y;
    
    Color LifeEnemyColor;
    
    int LifeDrainEnemy = LifeEnemy.x - (LifeSizeEnemyOriginal.x / MaxScore);
    
    const int ModelH_spriteWidth = 85;
    const int ModelH_spriteHeight = 85;
    
    Vector2 SelectionMarker;
    SelectionMarker.x = 20;
    SelectionMarker.y = 20;

    int ModelH_direction = 1;
    // 1. Right 2.Left
    int ModelH_animation = 5;
    /*
    1. Flying (Left)
    2. Dash (Left)
    3. Fly_Attack (Left)
    4. Dash_Attack (Left)
    5. Flying (Right)
    6. Dash (Right)
    7. Fly_Attack (Right)
    8. Dash_Attack (Right)
    */
    int ModelH_Dashing = 0;
    int ModelH_Attacking = 0;
    // 0. No 1. Yes
    
    Vector2 ModelH_Position;
    ModelH_Position.x = screenWidth/2;
    ModelH_Position.y = screenHeight/2; - 150;
    
    Rectangle ModelH_sourceRec;
    ModelH_sourceRec.x = 0;
    ModelH_sourceRec.y = 0;
    ModelH_sourceRec.width = ModelH_spriteWidth;
    ModelH_sourceRec.height = ModelH_spriteHeight;
   
    Rectangle ModelH_destinationRec;
    ModelH_destinationRec.width = ModelH_sourceRec.width;
    ModelH_destinationRec.height = ModelH_sourceRec.height;
    
    Rectangle ModelH_DestinationRec;
    ModelH_destinationRec.width = ModelH_spriteWidth/2;
    ModelH_destinationRec.height = screenHeight/2 - ModelH_spriteHeight/2;
   
    Vector2 ModelH_origin;
    ModelH_origin.x = 0;
    ModelH_origin.y = 0;
   
    int ModelH_framesCounter = 0;
    int ModelH_Dash_framesCounter = 0;
    int ModelH_Attack_framesCounter = 0;
    
    int ModelH_Speed = 3;
    int ModelH_MaxSpeed = 3;
    int ModelH_DashSpeed = 9;
    int ModelH_MaxDashSpeed = 9;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Font fontTtf = LoadFontEx("Resources/04B_30__.ttf", 30, 0, 250);
   
    Texture2D ModelH_Flying_Left = LoadTexture("Resources/Model-H/Model-H-Flying-Left-85x85-B&W.png");
    Texture2D ModelH_Flying_Right = LoadTexture("Resources/Model-H/Model-H-Flying-Right-85x85-B&W.png");
    Texture2D ModelH_Dash_Left = LoadTexture("Resources/Model-H/Model-H-Dash-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Right = LoadTexture("Resources/Model-H/Model-H-Dash-Right-85x85-B&W.png");
    Texture2D ModelH_Flying_Attacking_Left = LoadTexture("Resources/Model-H/Model-H-Flying-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Flying_Attacking_Right = LoadTexture("Resources/Model-H/Model-H-Flying-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Attacking_Left = LoadTexture("Resources/Model-H/Model-H-Dash-Attacking-Left-85x85-B&W.png");
    Texture2D ModelH_Dash_Attacking_Right = LoadTexture("Resources/Model-H/Model-H-Dash-Attacking-Right-85x85-B&W.png");
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                framesCounter++;
                
                if (LogoColor.a < 255){
                    LogoColor.a++;
                }
                    
                    if (framesCounter > 180){
                        screen = TITLE;
                        framesCounter = 0;
                    }
               
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                framesCounter++;
                if (framesCounter % 30 == 0){
                    framesCounter = 0;
                    blink = !blink;
                }
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
                    screen = MENU;
                }
                
            } break;
            case MENU:
            {
                if (counter <= 10){
                    counter++;
                }else{
                
                       if (IsKeyPressed(KEY_DOWN)){
                           TitleSelection++;
                       }
                       
                       if (IsKeyPressed(KEY_UP)){
                           TitleSelection--;
                       }
                       
                       if (TitleSelection < 1){
                           TitleSelection = 1;
                       }else if(TitleSelection > 4){
                           TitleSelection = 4;
                       }
                       
                           if (TitleSelection == 1){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = GAMEPLAY;
                                   counter = 0; 
                               }
                           }
                           
                           if (TitleSelection == 2){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = OPTIONS;
                                   counter = 0;
                                   OptionSelection = 1;
                               }
                           }
                           
                           if (TitleSelection == 3){
                               if (IsKeyPressed(KEY_ENTER)){
                                   screen = CONTROLS;
                                   counter = 0;
                                   ControlSelection = 1;
                               }
                           }
                          
                           if (TitleSelection == 4){
                               if (IsKeyPressed(KEY_ENTER)){
                                   return 0;
                               }
                           }
                }
            }break;
            
            case OPTIONS:
            {
                if (counter <= 10){
                    counter++;
                }else{
                   if (OptionSelection < 1){
                       OptionSelection = 1;
                   }else if(OptionSelection > 4){
                       OptionSelection = 4;
                   }

                   if (IsKeyPressed(KEY_DOWN)){
                       OptionSelection++;
                   }
                   if (IsKeyPressed(KEY_UP)){
                       OptionSelection--;
                   }
                   
                   if (OptionSelection < 1){
                       OptionSelection = 1;
                   }else if(OptionSelection > 4){
                       OptionSelection = 4;
                   }
                   
                       if (OptionSelection == 1){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameMode = 1;
                           }
                       }else if (OptionSelection == 2){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameDifficulty++;
                               if (GameDifficulty > 3){
                                   GameDifficulty = 1;
                               }
                           }
                       }else if (OptionSelection == 3){
                           if (IsKeyPressed(KEY_ENTER)){
                               GameMode = 2;;
                           }
                       }else if (OptionSelection == 4){
                           if (IsKeyPressed(KEY_ENTER)){
                               screen = MENU;
                               counter = 0;
                           }
                       }
                }   
            }break;
            case CONTROLS:
            {
                if (counter <= 10){
                    counter++;
                }else{
                    if (ControlSelection == 1){
                        if (IsKeyPressed(KEY_ENTER)){
                            screen = MENU;
                        }
                    }
                }
            }break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                
                
              if (!pause){
                // TODO: Ball movement logic.........................(0.2p)
                
                ball.x += ballSpeed.x;
                ball.y += ballSpeed.y;
                
                if (ball.x <= 0){
                    IAScore ++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballSpeed.x = 5;
                    ballSpeed.y = 5;
                }else if (ball.x >= screenWidth){
                    PlayerScore ++;
                    ball.x = screenWidth/2;
                    ball.y = screenHeight/2;
                    ballSpeed.x = 5;
                    ballSpeed.y = 5;
                }
                
                if (ballSpeed.x >= MaxSpeed){
                    ballSpeed.x = MaxSpeed;
                }else if (ballSpeed.y >= MaxSpeed){
                    ballSpeed.y = MaxSpeed;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                
                if (IsKeyDown(KEY_Q)){
                    LeftPaddle.y -= playerSpeedY;
                }else if (IsKeyDown(KEY_A)){
                    LeftPaddle.y += playerSpeedY;
                }
                
                if (IsKeyDown(KEY_UP)){
                    RightPaddle.y -= playerSpeedY;
                }else if (IsKeyDown(KEY_DOWN)){
                    RightPaddle.y += playerSpeedY;
                }
               
                if (LeftPaddle.y < 0){
                    LeftPaddle.y = 0;
                }else if (LeftPaddle.y > screenHeight){
                    LeftPaddle.y = screenHeight;
                }
                
                if (RightPaddle.y < 0){
                    RightPaddle.y = 0;   
                }else if (RightPaddle.y > screenHeight){
                    RightPaddle.y = screenHeight;
                }

                // TODO: Enemy movement logic (IA)...................(1p)
                
                if (ball.x > IALine){
                    if (ball.y > RightPaddle.y){
                        RightPaddle.y += IAspeed;
                    }else if (ball.y < RightPaddle.y){
                        RightPaddle.y -= IAspeed;
                    }
                }
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){LeftPaddle.x - PaddleSize.x/2, LeftPaddle.y - PaddleSize.y/2, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x < 0){
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.5;
                            ballSpeed.y *= 1.5;
                        }else{
                            ballSpeed.x *= -1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                if(CheckCollisionCircleRec(ball, ballRadius, (Rectangle){RightPaddle.x - PaddleSize.x/2, RightPaddle.y - PaddleSize.y/2, PaddleSize.x, PaddleSize.y})){
                    if (ballSpeed.x > 0){
                        if (abs(ballSpeed.x) < MaxSpeed){
                            ballSpeed.x *= -1.5;
                            ballSpeed.y *= 1.5;
                        }else{
                            ballSpeed.x *= -1;
                        }
                    }
                }
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                if (ball.y >= screenHeight - ballRadius - WallSize.y || ball.y < 0 + ballRadius + WallSize.y){
                    ballSpeed.y *= -1;
                }
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                //Model H programming    
                
                //Model H AI + collision detection
                    
                    //Movement/Attack logic (flying)
                     
                    //Dash logic
                    
                    //Dash Attack logic
                
                    //Speed limters
                
                //Model H animations programming
                      
                //------------------------------------------------------------------------------------------------------------------------------------------------------
                
                // TODO: Life bars decrease logic....................(1p)
                
                if (ball.x <= 0){
                    LifePlayer.x -= LifeDrainPlayer;
                }else if (ball.x >= screenWidth){
                    LifeEnemy.x -= LifeDrainEnemy;
                }
                
                if (LifePlayer.x >= LifeSizePlayerOriginal.x){
                    LifePlayerColor = GREEN;
                }else if (LifePlayer.x < LifeSizePlayerOriginal.x && LifePlayer.x > LifeSizePlayerOriginal.x/(MaxScore/4)){
                    LifePlayerColor = YELLOW;
                }else if (LifePlayer.x >= LifeSizePlayerOriginal.x/(MaxScore/4)){
                    LifePlayerColor = RED;
                }
                
                if (LifeEnemy.x >= LifeSizeEnemyOriginal.x){
                    LifeEnemyColor = GREEN;
                }else if (LifeEnemy.x < LifeSizeEnemyOriginal.x && LifeEnemy.x > LifeSizeEnemyOriginal.x/(MaxScore/4)){
                    LifeEnemyColor = YELLOW;
                }else if (LifeEnemy.x >= LifeSizeEnemyOriginal.x/(MaxScore/4)){
                    LifeEnemyColor = RED;
                }
                
                // TODO: Time counter logic..........................(0.2p)
                
                if (secondsCounter > 0){
                    framesCounter++;
                    if (framesCounter == 60){
                    secondsCounter--;
                    framesCounter = 0;
                    }
                }else{
                    screen = ENDING;
                }
                
                // TODO: Game ending logic...........................(0.2p)
                
                if (PlayerScore >= MaxScore || IAScore >= MaxScore){
                    screen = ENDING;
                }else if (secondsCounter <= 0){
                    screen = ENDING;
                }
              }
              // TODO: Pause button logic..........................(0.2p)
              if (IsKeyPressed(KEY_P)){
                    pause = !pause;
                }

            } break;
            case ENDING: 
            {
                // Update END screen data here!

                // TODO: Replay / Exit game logic....................(0.5p)
                
                if (IsKeyPressed(KEY_R)){
                    screen = GAMEPLAY;
                    pause = false;
                    PlayerScore = 0;
                    IAScore = 0;
                    ballSpeed.x = 8;
                    ballSpeed.y = 8;
                }else if (IsKeyPressed(KEY_ENTER)){
                    return 0;
                }
                
            } break;
            default: break;
        }
        
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawText("Roger Font", 160, 200, 80, LogoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawText("FINAL PONG", 150, 150, 80, LogoColor);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (!blink){
                        DrawText("PRESS ENTER TO START", 160, 250, 40, LogoColor);
                    }
                    
                } break;
                case MENU:
                {
                    DrawTextEx(fontTtf, FormatText("PONG"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 100, 0, MenusTextColor);
                    DrawTextEx(fontTtf, FormatText("by Roger Font"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 60}, 20, 0, MenusTextColor);
                    DrawText("Start game", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                    DrawText("Options", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                    DrawText("Controls", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    DrawText("Exit Game", screenWidth/2 - 200, screenHeight/2 + 200, 20, MenusTextColor);
                    
                    if (TitleSelection == 1){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 50}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 2){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 100}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 3){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }else if (TitleSelection == 4){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 200}, SelectionMarker, MenusTextColor);
                    }    
                }break;
                case OPTIONS:
                {
                    DrawTextEx(fontTtf, FormatText("OPTIONS"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 60, 0, MenusTextColor);
                    DrawText("Back", screenWidth/2 - 200, screenHeight/2 + 200, 20, MenusTextColor);
                    
                    if (GameMode == 1){
                        DrawText("Mode Select: Vs AI (Selected)", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                        DrawText("Mode Select: Multiplayer", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    }else if (GameMode == 2){
                        DrawText("Mode Select: Vs AI", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                        DrawText("Mode Select: Multiplayer (Selected)", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    }
                    
                    if (GameDifficulty == 1){
                            DrawText("AI Difficulty: Easy", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }else if (GameDifficulty == 2){
                            DrawText("AI Difficulty: Normal", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }else if (GameDifficulty == 3){
                            DrawText("AI Difficulty: Hard", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                        }
                    
                    if (OptionSelection == 1){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 50}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 2){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 100}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 3){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }else if (OptionSelection == 4){
                        DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 200}, SelectionMarker, MenusTextColor); 
                    }    
                }break;
                case CONTROLS:
                {
                    DrawTextEx(fontTtf, FormatText("CONTROLS"), (Vector2){screenWidth/2 - 200, screenHeight/2 - 180}, 60, 0, MenusTextColor);
                    DrawText("P1: Up arrow = up / Down arrow = down", screenWidth/2 - 200, screenHeight/2 + 50, 20, MenusTextColor);
                    DrawText("P2: Q = up / A = down", screenWidth/2 - 200, screenHeight/2 + 100, 20, MenusTextColor);
                    DrawText("Back", screenWidth/2 - 200, screenHeight/2 + 150, 20, MenusTextColor);
                    if (ControlSelection == 1){
                    DrawRectangleV((Vector2){screenWidth/2 - 250, screenHeight/2 + 150}, SelectionMarker, MenusTextColor);
                    }
                }break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawTextEx(fontTtf, FormatText("%d", secondsCounter), (Vector2){screenWidth/2 - 20, 20}, 50, 0, BLACK);
                    
                    DrawRectangleV(WallUp, WallSize, BLACK);
                    DrawRectangleV(WallDown, WallSize, BLACK);
                    
                    DrawCircleV(ball, ballRadius, BLACK);
                    
                    // Model H sprites:
                    if (ModelH_animation == 1){
                        DrawTexturePro(ModelH_Flying_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 2){
                        DrawTexturePro(ModelH_Dash_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 3){
                        DrawTexturePro(ModelH_Flying_Attacking_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 4){
                        DrawTexturePro(ModelH_Dash_Left, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 5){
                        DrawTexturePro(ModelH_Flying_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 6){
                        DrawTexturePro(ModelH_Dash_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 7){
                        DrawTexturePro(ModelH_Flying_Attacking_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }else if (ModelH_animation == 8){
                        DrawTexturePro(ModelH_Dash_Attacking_Right, ModelH_sourceRec, ModelH_destinationRec, ModelH_origin, 0, WHITE);
                    }
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    DrawRectangleV(LeftPaddle, PaddleSize, BLACK);
                    DrawRectangleV(RightPaddle, PaddleSize, BLACK);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleV(LifePlayer, LifeSizePlayer, LifePlayerColor);
                    DrawRectangleV(LifeEnemy, LifeSizeEnemy, LifeEnemyColor);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pause == true){
                        DrawText("Game paused, press P to continue", 150, 150, 30, BLACK);
                    }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    if (PlayerScore >= MaxScore){
                        DrawText("Player WINS!", 150, 150, 30, BLACK);
                        DrawText("Press R to do another match!", 150, 200, 30, BLACK);
                        DrawText("or Press Enter close the game", 150, 250, 30, BLACK);
                    }else if (IAScore >= MaxScore){
                        DrawText("AI WINS!", 150, 150, 30, BLACK);
                        DrawText("Press R to do another match!", 150, 200, 30, BLACK);
                        DrawText("or Press Enter close the game", 150, 250, 30, BLACK);
                    }
                    
                    if (secondsCounter <= 0){
                        DrawText("TIME RAN OUT!", 150, 150, 30, BLACK);
                    }
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    UnloadFont (fontTtf);
    UnloadTexture(ModelH_Flying_Left);
    UnloadTexture(ModelH_Flying_Right);
    UnloadTexture(ModelH_Dash_Left);
    UnloadTexture(ModelH_Dash_Right);
    UnloadTexture(ModelH_Flying_Attacking_Left);
    UnloadTexture(ModelH_Flying_Attacking_Right);
    UnloadTexture(ModelH_Dash_Attacking_Left);
    UnloadTexture(ModelH_Dash_Attacking_Right);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}